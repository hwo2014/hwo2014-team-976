#ifndef HWO_GAME_LOGIC_H
#define HWO_GAME_LOGIC_H

#include <string>
#include <vector>
#include <map>
#include <functional>
#include <iostream>
#include <jsoncons/json.hpp>

struct road_piece {
	bool isBend;
	bool isSwitchable;
	double length;
	double radius;
	double angle;
};

struct bend_seg {
	double angle;
	double radius;
	int l, r;
};

struct car {
	//basic info
	std::string name;
	std::string color;
	double length;
	double width;
	double guideFlag;
	//race info
	float angle;
	int pieceIndex;
	double inPieceDistance;
	int startLaneIndex;
	int endLaneIndex;
	int lap;

	car& operator =(const car& a)
	{
		name = a.name;
		color = a.color;
		length = a.length;
		width = a.width;
		guideFlag = a.guideFlag;
		angle = a.angle;
		pieceIndex = a.pieceIndex;
		inPieceDistance = a.inPieceDistance;
		startLaneIndex = a.startLaneIndex;
		endLaneIndex = a.endLaneIndex;
		lap = a.lap;
		return *this;
	}
};

class game_logic
{
public:
  typedef std::vector<jsoncons::json> msg_vector;

  game_logic();
  msg_vector react(const jsoncons::json& msg);

private:
  typedef std::function<msg_vector(game_logic*, const jsoncons::json&)> action_fun;
  const std::map<std::string, action_fun> action_map;

  msg_vector on_join(const jsoncons::json& data);
  msg_vector on_your_car(const jsoncons::json& data);
  msg_vector on_game_start(const jsoncons::json& data);
  msg_vector on_game_init(const jsoncons::json& data);
  msg_vector on_car_positions(const jsoncons::json& data);
  msg_vector on_crash(const jsoncons::json& data);
  msg_vector on_game_end(const jsoncons::json& data);
  msg_vector on_error(const jsoncons::json& data);
  msg_vector on_turbo(const jsoncons::json& data);
  msg_vector on_lapfinished(const jsoncons::json& data);


  double getDesireSpeedInBend(double angle, double radius);
  double DistanceNeedToSlowDown(double speed, double desiredSpeed);
  void ClearData();

  std::vector<bend_seg> m_Bends;
  std::vector<road_piece> m_RoadPieces;
  std::vector<float> m_Lanes;
  std::vector<car> m_Cars;
  car m_MyCar;
  int m_MyCarIndex;
  int m_LastPieceIndex;
  double m_LastPieceDistance;
  double m_TotalDistance;
  int m_LastGameTick;
  int m_GameTick;
  double m_CurThrotle;
  double m_Velocity;
  double m_Acceleration;
  std::vector<int> m_BendAngle;
  std::vector<int> m_BendRadius;
  std::vector<int> m_BendIndex;
  std::vector<double> m_BendVelocity;
  std::vector<double> m_RunAngle;
  int m_Sign, m_CurrentBend;
  double m_CurrentBendAngle;
  bool m_bStartDrift;
  bool m_HasTurbo;
  bool m_PassPeek;
  double accTick1;
  double accTick2;
  double m_MaxVelocity;
  double m_Reduce;
  bool m_bSlowingDownToBend;
  double m_Magic;
  bool m_ShouldReply;
};

#endif
