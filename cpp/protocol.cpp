#include "protocol.h"

namespace hwo_protocol
{

  jsoncons::json make_request(const std::string& msg_type, const jsoncons::json& data)
  {
    jsoncons::json r;
    r["msgType"] = msg_type;
    r["data"] = data;
    return r;
  }

  jsoncons::json make_join(const std::string& name, const std::string& key)
  {
    jsoncons::json data;
    data["name"] = name;
    data["key"] = key;
    return make_request("join", data);
  }

  jsoncons::json make_joinRace(const std::string& name, const std::string& key, const std::string& track, const int carcount)
  {
    jsoncons::json data;
	jsoncons::json info;
	info["name"] = name;
	info["key"] = key;
	data["botId"] = info;
	data["trackName"] = track;
	data["carCount"] = carcount;
    return make_request("joinRace", data);
  }

  jsoncons::json make_ping()
  {
    return make_request("ping", jsoncons::null_type());
  }

  jsoncons::json make_throttle(double throttle, int tick)
  {
    //return make_request("throttle", throttle);
	jsoncons::json r;
    r["msgType"] = "throttle";
    r["data"] = throttle;
	//r["gameTick"] = tick;
    return r;
  }

   jsoncons::json make_switchlane(std::string direction)
  {
    return make_request("switchLane", direction);
  }

      jsoncons::json make_useturbo()
  {
    return make_request("turbo", "Chocobo");
  }

}  // namespace hwo_protocol
