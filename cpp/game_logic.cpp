#include "game_logic.h"
#include "protocol.h"

using namespace hwo_protocol;

game_logic::game_logic()
  : action_map
    {
      { "join", &game_logic::on_join },
	  { "yourCar", &game_logic::on_your_car },
	  { "gameInit", &game_logic::on_game_init },
      { "gameStart", &game_logic::on_game_start },
      { "carPositions", &game_logic::on_car_positions },
      { "crash", &game_logic::on_crash },
      { "gameEnd", &game_logic::on_game_end },
      { "error", &game_logic::on_error },
	  { "turboAvailable", &game_logic::on_turbo },
	   { "lapFinished", &game_logic::on_lapfinished }
    }
{
  m_LastPieceIndex = 0;
  m_LastPieceDistance = 0;
  m_TotalDistance = 0;
  m_CurThrotle = 0;
  m_Velocity = 0;
  m_Acceleration = 0;
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
  m_MyCarIndex = 0;
  const auto& msg_type = msg["msgType"].as<std::string>();
  //std::cout << msg_type << std::endl;
  const auto& data = msg["data"];
  m_ShouldReply = false;
  if (msg_type == "carPositions" && msg.has_member("gameTick")) {
	  m_LastGameTick = m_GameTick;
	  m_GameTick = msg["gameTick"].as<int>();
	  m_ShouldReply = true;
	  //std::cout << "Tick: " << m_GameTick << " ";
  }
  auto action_it = action_map.find(msg_type);
  if (action_it != action_map.end())
  {
    return (action_it->second)(this, data);
  }
  else
  {
    std::cout << "Unknown message type: " << msg_type << std::endl;
    return {  };
  }
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
  std::cout << "Joined" << std::endl;
  return {  };
}

game_logic::msg_vector game_logic::on_your_car(const jsoncons::json& data)
{
  std::cout << "Your car" << std::endl;
  m_MyCar.name = data["name"].as<std::string>();
  m_MyCar.color = data["color"].as<std::string>();;
  return {  };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
  std::cout << "Race started" << std::endl;
  m_LastPieceIndex = 0;
  m_LastPieceDistance = 0;
  m_TotalDistance = 0;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data)
{
  ClearData();
  std::cout << "Game init" << std::endl;
  std::cout << "Track id: " << data["race"]["track"]["id"] << std::endl;
  std::cout << "Track name: " << data["race"]["track"]["id"] <<  std::endl;
  std::cout << "Track pieces num: " << data["race"]["track"]["pieces"].size() << std::endl;
  for (int i = 0; i < data["race"]["track"]["pieces"].size(); i++) {
	  road_piece rp;
	  if (data["race"]["track"]["pieces"][i].has_member("radius")) {
		  rp.isBend = true;
		  rp.radius = data["race"]["track"]["pieces"][i]["radius"].as<double>();
		  rp.angle = data["race"]["track"]["pieces"][i]["angle"].as<double>();
		  rp.isSwitchable = false;
		  rp.length = 2 * 3.14f * rp.radius * (abs(rp.angle) / (double)360);
	  } else {
		  rp.isBend = false;
		  rp.length = data["race"]["track"]["pieces"][i]["length"].as<double>();
		  if (data["race"]["track"]["pieces"][i].has_member("switch")) {
			  rp.isSwitchable = data["race"]["track"]["pieces"][i]["switch"].as<bool>();
		  } else {
			  rp.isSwitchable = false;
		  }
		  rp.radius = 0;
		  rp.angle = 0;
	  }
	  m_RoadPieces.push_back(rp);
	  std::cout << rp.isBend << " " << rp.isSwitchable << " " << rp.length << " " << rp.angle << " " << rp.radius << std::endl;
  }
  if (data["race"]["track"].has_member("cars")) {
		for (int i = 0; i < data["race"]["track"]["cars"].size(); i++) {
			car ca;
			ca.name = data["race"]["track"]["cars"][i]["id"]["name"].as<std::string>();
			ca.color = data["race"]["track"]["cars"][i]["id"]["color"].as<std::string>();
			ca.length = data["race"]["track"]["cars"][i]["dimensions"]["length"].as<double>();
			ca.width = data["race"]["track"]["cars"][i]["dimensions"]["width"].as<double>();
			ca.guideFlag = data["race"]["track"]["cars"][i]["dimensions"]["guideFlagPosition"].as<double>();
			m_Cars.push_back(ca);
			if (ca.name == m_MyCar.name) {
				m_MyCarIndex = i;
				m_MyCar = ca;
			}
	  }
  } else {
	  m_Cars.push_back(m_MyCar);
	  m_MyCarIndex = 0;
  }

  bool bEnd = false;
  int i = 0;
  while (!bEnd) {
	  
	  while (!m_RoadPieces[i].isBend && i < m_RoadPieces.size()) {
		  i++;
	  }
	  double radius = m_RoadPieces[i].radius;
	  double angle = m_RoadPieces[i].angle;
	  int ni = i + 1;
	  if (ni >= m_RoadPieces.size()) {
		  ni = 0;
		  bEnd = true;
	  }
	  if (radius == 0) {
		  break;
	  }
	  m_BendRadius.push_back(radius);
	  m_BendIndex.push_back(i);
	  while (m_RoadPieces[ni].isBend && m_RoadPieces[ni].radius == radius && m_RoadPieces[ni].angle * angle > 0) {
		  angle += m_RoadPieces[ni].angle;
		  i = ni;
		  ni = i + 1;
		  if (ni > m_RoadPieces.size()) {
			  ni = 0;
			  bEnd = true;
		  }
	  }
	  m_BendAngle.push_back(angle);
	  m_BendVelocity.push_back(getDesireSpeedInBend(angle, radius));
	  i = i + 1;
	  if (i >= m_RoadPieces.size()) {
		i = 0;
		bEnd = true;
	  }
	  m_RunAngle.push_back(0);
  }



  m_BendVelocity[3] = 6.6;
  m_BendVelocity[4] = 6.8;
  m_BendVelocity[7] = 7.0;
  m_BendVelocity[8] = 6.7;
  m_BendVelocity[9] = 6.9;

  for (int i = 0; i < m_BendRadius.size(); i++) {
	  std::cout << "Bend: " << i << " Radius: " << m_BendRadius[i] << " Angle: " << m_BendAngle[i] << " Index: " << m_BendIndex[i] << " Speed: " << m_BendVelocity[i] << std::endl;
  }

  return {  };
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
	//std::cout << "Car index: " << m_MyCarIndex << std::endl;
	for (int i = 0; i < data.size(); i++) {
		try {
		m_Cars[i].angle = data[i]["angle"].as<double>();
		m_Cars[i].pieceIndex = data[i]["piecePosition"]["pieceIndex"].as<int>();
		m_Cars[i].inPieceDistance = data[i]["piecePosition"]["inPieceDistance"].as<double>();
		//std::cout << i << " distance " << m_Cars[i].inPieceDistance << std::endl;
		m_Cars[i].startLaneIndex = data[i]["piecePosition"]["lane"]["startLaneIndex"].as<int>();
		m_Cars[i].endLaneIndex = data[i]["piecePosition"]["lane"]["endLaneIndex"].as<int>();
		if (data[i].has_member("lap")) {
			m_Cars[i].lap = data[i]["lap"].as<int>();
		}
		if (i == m_MyCarIndex) {
			//std::cout << "YOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO";
			m_MyCar = m_Cars[i];
		}
		} catch (...) {
		}
	}

	int r = 0;

	float distanceToBend = 0;
	int pidx = m_MyCar.pieceIndex;
	while (m_RoadPieces[pidx].isBend) {
		if (pidx == m_MyCar.pieceIndex) {
			distanceToBend += (m_RoadPieces[pidx].length - m_MyCar.inPieceDistance);
		} else {
			distanceToBend += m_RoadPieces[pidx].length;
		}
		int nxt = pidx + 1;
		if (nxt >= m_RoadPieces.size()) {
			nxt = 0;
		}
		if (m_RoadPieces[nxt].radius == m_RoadPieces[pidx].radius && (m_RoadPieces[nxt].angle  * m_RoadPieces[pidx].angle > 0)) {
			pidx = nxt;
		} else {
			break;
		}
	}
	while (!m_RoadPieces[pidx].isBend) {
		if (pidx == m_MyCar.pieceIndex) {
			distanceToBend += (m_RoadPieces[pidx].length - m_MyCar.inPieceDistance);
		} else {
			distanceToBend += m_RoadPieces[pidx].length;
		}
		pidx++;
		if (pidx >= m_RoadPieces.size()) {
			pidx = 0;
		}
	}

	int bendAngle = m_RoadPieces[pidx].angle;
	int bendRadius = m_RoadPieces[pidx].radius;
	while (m_RoadPieces[pidx].isBend) {
		int nidx = pidx + 1;
		if (nidx >= m_RoadPieces.size()) {
			nidx = 0;
		}
		if (m_RoadPieces[pidx].angle * m_RoadPieces[nidx].angle > 0) {
			pidx = nidx;
			bendAngle += m_RoadPieces[pidx].angle;
		} else {
			pidx++;
		}
	}

	double velocity = 0;
	bool newPiece = false;
	if (m_LastPieceIndex == m_MyCar.pieceIndex) {
		velocity = m_MyCar.inPieceDistance - m_LastPieceDistance;
	} else {
		newPiece = true;
		if (m_MyCar.pieceIndex == 35 && m_HasTurbo) {
			m_HasTurbo = false;
			return { make_useturbo() };
		}
		velocity = (m_RoadPieces[m_LastPieceIndex].length - m_LastPieceDistance) + m_MyCar.inPieceDistance;
		for (int i = 0; i < m_BendIndex.size(); i++) {
			if (m_BendIndex[i] == m_MyCar.pieceIndex) {
				int prev = i - 1;
				if (prev < 0) {
					prev = m_BendIndex.size() - 1;
				}
				m_RunAngle[prev] = m_CurrentBendAngle;
				std::cout << "Bend: " << prev << " Angle: " << m_CurrentBendAngle << std::endl;
				if (m_BendAngle[i] > 0) {
					m_Sign = 1;
				} else {
					m_Sign = -1;
				}
				m_PassPeek = false;
				m_CurrentBendAngle = m_MyCar.angle;
				m_bStartDrift = false;
				m_CurrentBend = i;
				break;
			
			}
		}
		if (m_RoadPieces[m_MyCar.pieceIndex].isBend) {
			m_bSlowingDownToBend = false;
		}
	}

	if (m_Sign == 1) {
		if (m_CurrentBendAngle < m_MyCar.angle) {
			m_CurrentBendAngle = m_MyCar.angle;
			m_bStartDrift = true;
			//std::cout << "Start drift" << std::endl;
		} else {
			if (!m_bStartDrift) {
				m_CurrentBendAngle = m_MyCar.angle;
			} else {
				if (m_CurrentBendAngle > m_MyCar.angle && m_MyCar.angle > 0) {
					//std::cout << "Pass Peek" << std::endl;
					m_PassPeek = true;
				}
			}
		}
	} else if (m_Sign == -1) {
		if (m_CurrentBendAngle > m_MyCar.angle) {
			m_CurrentBendAngle = m_MyCar.angle;
			m_bStartDrift = true;
			//std::cout << "Start drift" << std::endl;
		} else {
			if (!m_bStartDrift) {
				m_CurrentBendAngle = m_MyCar.angle;
			} else {
				if (m_CurrentBendAngle < m_MyCar.angle && m_MyCar.angle < 0) {
					//std::cout << "Pass Peek" << std::endl;
					m_PassPeek = true;
				}
			}
		}
	}

	velocity = velocity / (m_GameTick - m_LastGameTick);
	m_Acceleration = velocity - m_Velocity;
	m_Velocity = velocity;

	m_TotalDistance += velocity;
	m_LastPieceIndex = m_MyCar.pieceIndex;
	m_LastPieceDistance = m_MyCar.inPieceDistance;

	
	std::cout << "Tick: " << m_GameTick << " Speed: " << velocity << " Angle: " << m_MyCar.angle << " In piece: " << m_MyCar.inPieceDistance << " Piece: " << m_MyCar.pieceIndex << " Acc: " << m_Acceleration << " Throttle: " << m_CurThrotle << std::endl;
	if (m_GameTick == 2) {
		accTick1 = m_Acceleration;
	}
	if (m_GameTick == 3) {
		accTick2 = m_Acceleration;
		m_Reduce = accTick2 / accTick1;
		double sa = accTick1;
		m_MaxVelocity = 0;
		m_Magic = 0;
		double ma = 1; 
		for (int i = 0; i < 1000; i++) {
			m_MaxVelocity += sa;
			sa *= m_Reduce;
			m_Magic += ma;
			ma *= m_Reduce;
		}
		std::cout << "MaxVelocity: " << m_MaxVelocity << " Magic: " << m_Magic << std::endl;
	}

	jsoncons::json ret;
	jsoncons::json ret1;
	bool bswitch = false;

	if (newPiece) {
		if (m_MyCar.pieceIndex == 2 && m_MyCar.startLaneIndex == 0) {
			ret1 = make_switchlane("Right");
			bswitch = true;
		}
		if (m_MyCar.pieceIndex == 5 && m_MyCar.startLaneIndex == 1) {
			ret1 = make_switchlane("Left");
			bswitch = true;
		}
		if (m_MyCar.pieceIndex == 15 && m_MyCar.startLaneIndex == 0) {
			ret1 = make_switchlane("Right");
			bswitch = true;
		}
	}

	double desiredSpeed;
	int idx = m_MyCar.pieceIndex;
	if (m_RoadPieces[idx].isBend) {
		bool found = false;
		for (int i = 0; i < m_BendVelocity.size(); i++) {
			if (m_BendIndex[i] > idx) {
				int p = i -1;
				if (p < 0) {
					p = m_BendIndex.size() - 1;
				}
				desiredSpeed = m_BendVelocity[p];
				found = true;
				break;
			}
		}
		if (!found) {
			desiredSpeed = m_BendVelocity[0];
		}
	} else {
		bool found = false;
		for (int i = 0; i < m_BendVelocity.size(); i++) {
			if (m_BendIndex[i] > idx) {
				desiredSpeed = m_BendVelocity[i];
				found = true;
				break;
			}
		}
		if (!found) {
			desiredSpeed = m_BendVelocity[0];
		}
	}

	if (m_RoadPieces[m_MyCar.pieceIndex].isBend && !m_PassPeek) {
		//std::cout << "Distance to Bend: " << distanceToBend << " Bend Angle: " << bendAngle << " Bend Radius " << bendRadius << std::endl;
		//double desiredSpeed = getDesireSpeedFromIndex(m_CurBendAngle, m_CurBendRadius);
		if (m_Velocity < desiredSpeed) {
			m_CurThrotle = desiredSpeed / m_MaxVelocity;
		} else {
			m_CurThrotle = 0.0f;
		}
	} else {
		//std::cout << "Distance to Bend: " << distanceToBend << " Bend Angle: " << bendAngle << " Bend Radius " << bendRadius << std::endl;
		//double desiredSpeed = getDesireSpeedInBend(bendAngle, bendRadius);
		//if (m_Velocity < desiredSpeed) {
		//	m_CurThrotle = 1.0f;
		//} else {
			//if (distanceToBend > 100) {
			//	m_CurThrotle = 1.0f;
			//} else {
			//	if (m_Velocity < desiredSpeed) {
			//		m_CurThrotle = desiredSpeed / m_MaxVelocity;
			//	} else {
			//		m_CurThrotle = 0.0f;
			//	}
			//}
		if (!m_bSlowingDownToBend && m_GameTick > 0 &&! newPiece) {
				double distanceNeed = DistanceNeedToSlowDown(m_Velocity, desiredSpeed);
				if (distanceNeed > distanceToBend) {
					m_bSlowingDownToBend = true;
					//std::cout << "Distance need: " << distanceNeed << std::endl;
				}
			}
			if (!m_bSlowingDownToBend) {
				m_CurThrotle = 1.0f;
			} else {
				if (m_Velocity < desiredSpeed) {
					m_CurThrotle = desiredSpeed / m_MaxVelocity;
				} else {
					m_CurThrotle = 0.0f;
				}
			}
			

		//}
	}

	if (m_CurThrotle > 1.0f) {
		m_CurThrotle = 1.0f;
	}
	if (m_CurThrotle < 0.0f) {
		m_CurThrotle = 0.0f;
	}

	//if (m_GameTick < 10) {
	//	m_CurThrotle = 1.0f;
	//} else if (m_GameTick < 20) {
	//	m_CurThrotle = 0.0f;
	//} else {
	//	m_CurThrotle = 1.0f;
	//}
	ret = make_throttle(m_CurThrotle, m_GameTick);

	if (m_GameTick == 1) {
		m_CurThrotle = 1.0f;
	} 
 //if (m_GameTick == 2) {
	// return { make_switchlane("Right") };
  //} else {
	if (m_ShouldReply) {
		if (bswitch) {
			return { ret1 };
		} else {
			return {ret};
		}
	} else {
		return {};
	}
  //}
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
  std::cout << "Someone crashed" << std::endl;
  return {  };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
  std::cout << "Race ended" << std::endl;
  return {  };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
  std::cout << "Error: " << data.to_string() << std::endl;
  return {  };
}

game_logic::msg_vector game_logic::on_turbo(const jsoncons::json& data)
{
  std::cout << "Turbo " << data.to_string() << std::endl;
  m_HasTurbo = true;
  return {  };
}

game_logic::msg_vector game_logic::on_lapfinished(const jsoncons::json& data)
{
	std::cout << "LapFinish:  " << data["lapTime"]["millis"].as<std::string>() << std::endl;
  return {  };
}

void game_logic::ClearData() {
  m_LastPieceIndex = 0;
  m_LastPieceDistance = 0;
  m_TotalDistance = 0;
  m_CurThrotle = 0;
  m_Velocity = 0;
  m_Acceleration = 0;
  m_BendAngle.clear();
  m_BendRadius.clear();
  m_BendIndex.clear();
  m_BendVelocity.clear();
  m_RunAngle.clear();
  m_Bends.clear();
  m_RoadPieces.clear();
  m_Lanes.clear();
  m_Cars.clear();
  m_bSlowingDownToBend = false;
  m_HasTurbo = false;
}

double game_logic::getDesireSpeedInBend(double angle, double radius) {
	//if (abs(angle) >= 135) {
	//	return 4.5f;
	//} else 
	//if (abs(angle) >= 90) {
	//	return 4.0f;
	//} else 
	//if (abs(angle) >= 45) {
	//	return 6.0f;
	//} else {
	//	return 7.0f;
	//}
	//if (abs(angle) >= 135) {
	//	return 6.5f;
	//} else 
	//if (abs(angle) >= 90) {
	//	return 6.0f;
	//} else 
	//if (abs(angle) >= 45) {
	//	return 6.0f;
	//} else {
	//	return 7.0f;
	//}

	if (abs(angle) < 45) {
		return 20;
	}
	if (radius <= 100) {
		return 6.6;
	} else if (radius <= 200) {
		return 9.0;
	}

}

double game_logic::DistanceNeedToSlowDown(double speed, double desiredSpeed) {
	double distance = 0;
	if (speed < desiredSpeed) {
		return 0;
	}
	double acc = (speed / m_Magic) / 1.00;
	while (speed > desiredSpeed) {
		distance += speed;
		speed -= acc;
		acc *= m_Reduce;
	}
	distance += speed;
	return distance;
}